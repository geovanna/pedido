<?php 
class Foo { 
    public $a = '4'; 
    public $b = '5'; 
    public $contenido = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in eleifend ipsum. Aenean a enim vestibulum, scelerisque ante sed, sagittis felis. Aliquam in luctus felis. Praesent aliquam felis et ipsum bibendum elementum. Ut tincidunt vehicula nulla eu suscipit. Nulla in est sed ipsum vulputate posuere eget dapibus justo. Pellentesque ac egestas felis. In pulvinar blandit ex ut pulvinar. Cras maximus diam scelerisque elit dictum efficitur.</p>';
    
    
    function leer() { 
        echo $this->contenido;
    } 

} 

$foo = new Foo; 
var_dump($foo->a = 100);
var_dump($foo->leer());

?> 