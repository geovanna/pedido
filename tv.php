<?php 
	class TVType
	{
	    const PLASMA = 'PLASMA';
		const LCD = 'LCD';
		const LED = 'LED';
		const CRT = 'CRT';
		const OLED = 'OLED';
		const CURVAS = 'PANTALLAS CURVAS';
	}

	class TVTam{
		const GRANDE = '65p';
		const MEDIANA = '40p';
		const PEQUENA = '20p';
	}

	class TVGestion{
		const T = '2007';
		const S = '2006';
		const R = '2005';
		const P = '2003';
		const M = '2002';
	}
	// cLase constructor and destructor
	class TV
	{
		private $type;
		private $haveColors;
		private $nombre;
		private $gestionF;
		private $tamano;


		public function __construct($color =true, $tipo = TVType::LED, $name = 'S/N', $gestion = TVGestion::T, $tam = TVTam::GRANDE){
		
			$this->haveColors = $color;
			$this->type = $tipo;
			$this->gestionF = $gestion;
			$this->tamano = $tam;
		
		}
		public function __destruct(){
			var_dump($this->haveColors);
			unset($this->haveColors);

		}

		public function printInfo(){
			echo 'Nombre:'.$this->nombre.'<br>';
			echo "colors:".(($this->haveColors)?"yes":"No").'<br>'; 
			echo "type:". $this->type;
		}

		public function set_nombre($name)
		{
			$this->nombre = $name;
		}

		public function get_nombre()
		{
			return $this->nombre;
		}
		
	}

	$panasonicL15 = new TV(false, TVType::PLASMA, 'bien');
	//$panasonicL15->set_nombre('peque');
	$panasonicL15->printInfo();